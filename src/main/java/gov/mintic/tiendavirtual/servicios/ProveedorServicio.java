package gov.mintic.tiendavirtual.servicios;

import gov.mintic.tiendavirtual.entidades.Proveedor;
import gov.mintic.tiendavirtual.interfaces.Operaciones;
import gov.mintic.tiendavirtual.repositorios.ProveedorRepositorio;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service("ProveServicio")
public class ProveedorServicio implements Operaciones<Proveedor>{
    
    @Autowired
    private ProveedorRepositorio proveedorRepo;

    @Override
    public List<Proveedor> consultar() {
        return proveedorRepo.obtenerTodas();
    }

    @Override
    public Boolean agregar(Proveedor miObjeto) {
        Proveedor temporal = proveedorRepo.save(miObjeto);
        return temporal != null;
    }

    @Override
    public Integer cantidadRegistros() {
        return proveedorRepo.cantidadTotal();
    }

    @Override
    public Boolean eliminar(Integer llavePrimaria) {
        proveedorRepo.deleteById(llavePrimaria);
        return !proveedorRepo.existsById(llavePrimaria);
    }

    @Override
    public Boolean actualizar(Proveedor miObjeto) {
        Optional<Proveedor> objetoVerificado = proveedorRepo.findById(miObjeto.getCodProveedor());
        if (!objetoVerificado.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Objeto no aceptado");
        } else {
            proveedorRepo.save(miObjeto);
            return true;
        }
    }

    @Override
    public Proveedor buscar(Integer llavePrimaria) {
        return proveedorRepo.findById(llavePrimaria).get();
    }
    
}
