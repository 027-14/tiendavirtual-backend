package gov.mintic.tiendavirtual.servicios;

import gov.mintic.tiendavirtual.entidades.Categoria;
import gov.mintic.tiendavirtual.interfaces.Operaciones;
import gov.mintic.tiendavirtual.repositorios.CategoriaRepositorio;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service("CategoServicio")
public class CategoriaServicio implements Operaciones<Categoria> {

    @Autowired
    private CategoriaRepositorio categoRepo;

    @Override
    public List<Categoria> consultar() {
        return categoRepo.obtenerTodas();
    }

    @Override
    public Boolean agregar(Categoria miObjeto) {
        Categoria temporal = categoRepo.save(miObjeto);
        return temporal != null;
    }

    @Override
    public Integer cantidadRegistros() {
        return categoRepo.cantidadTotal();
    }

    @Override
    public Boolean eliminar(Integer llavePrimaria) {
        categoRepo.deleteById(llavePrimaria);
        return !categoRepo.existsById(llavePrimaria);
    }

    @Override
    public Categoria buscar(Integer llavePrimaria) {
        return categoRepo.findById(llavePrimaria).get();
    }

    // No definidos en la interfaz
    // *************************************************************************
    public List<Categoria> consultarActivas(Short estado) {
        return categoRepo.obtenerActivas(estado);
    }

    public Integer cambiarEstado(Short nuevoEstado, Integer codigo) {
        return categoRepo.cambiarEstado(nuevoEstado, codigo);
    }

    @Override
    public Boolean actualizar(Categoria miObjeto) {
        Optional<Categoria> objetoVerificado = categoRepo.findById(miObjeto.getCodCategoria());
        if (!objetoVerificado.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Objeto no aceptado");
        } else {
            categoRepo.save(miObjeto);
            return true;
        }
    }

}
