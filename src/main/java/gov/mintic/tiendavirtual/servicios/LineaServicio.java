package gov.mintic.tiendavirtual.servicios;

import gov.mintic.tiendavirtual.entidades.Linea;
import gov.mintic.tiendavirtual.interfaces.Operaciones;
import gov.mintic.tiendavirtual.repositorios.LineaRepositorio;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service("LineaServicio")
public class LineaServicio implements Operaciones<Linea> {

    @Autowired
    private LineaRepositorio lineaRepo;

    @Override
    public List<Linea> consultar() {
        return lineaRepo.obtenerTodas();
    }

    @Override
    public Boolean agregar(Linea miObjeto) {
        Linea temporal = lineaRepo.save(miObjeto);
        return temporal != null;
    }

    @Override
    public Integer cantidadRegistros() {
        return lineaRepo.cantidadTotal();
    }

    @Override
    public Boolean eliminar(Integer llavePrimaria) {
        lineaRepo.deleteById(llavePrimaria);
        return !lineaRepo.existsById(llavePrimaria);
    }

    @Override
    public Boolean actualizar(Linea miObjeto) {
        Optional<Linea> objetoVerificado = lineaRepo.findById(miObjeto.getCodLinea());
        if (!objetoVerificado.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Objeto no aceptado");
        } else {
            lineaRepo.save(miObjeto);
            return true;
        }
    }

    @Override
    public Linea buscar(Integer llavePrimaria) {
        return lineaRepo.findById(llavePrimaria).get();
    }
    
    // No definidos en la interfaz
    // *************************************************************************
    public List<Linea> consultarActivas(Short estado) {
        return lineaRepo.obtenerActivas(estado);
    }

    public Integer cambiarEstado(Short nuevoEstado, Integer codigo) {
        return lineaRepo.cambiarEstado(nuevoEstado, codigo);
    }

}
