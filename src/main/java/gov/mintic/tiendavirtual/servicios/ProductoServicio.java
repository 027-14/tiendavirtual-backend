package gov.mintic.tiendavirtual.servicios;

import gov.mintic.tiendavirtual.entidades.Producto;
import gov.mintic.tiendavirtual.interfaces.Operaciones;
import gov.mintic.tiendavirtual.repositorios.ProductoRepositorio;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service("ProServicio")
public class ProductoServicio implements Operaciones<Producto>{
    
    @Autowired
    private ProductoRepositorio proRepo;

    @Override
    public List<Producto> consultar() {
        return proRepo.obtenerTodas();
    }

    @Override
    public Boolean agregar(Producto miObjeto) {
        Producto temporal = proRepo.save(miObjeto);
        return temporal != null;
    }

    @Override
    public Integer cantidadRegistros() {
        return proRepo.cantidadTotal();
    }

    @Override
    public Boolean eliminar(Integer llavePrimaria) {
        proRepo.deleteById(llavePrimaria);
        return !proRepo.existsById(llavePrimaria);
    }

    @Override
    public Boolean actualizar(Producto miObjeto) {
       Optional<Producto> objetoVerificado = proRepo.findById(miObjeto.getCodProducto());
        if (!objetoVerificado.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Objeto no aceptado");
        } else {
            proRepo.save(miObjeto);
            return true;
        }
    }

    @Override
    public Producto buscar(Integer llavePrimaria) {
        return proRepo.findById(llavePrimaria).get();
    }

    // No definidos en la interfaz
    // *************************************************************************
//    public List<Producto> consultarActivas(Short estado) {
//        return proRepo.obtenerActivas(estado);
//    }
//
//    public Integer cambiarEstado(Short nuevoEstado, Integer codigo) {
//        return proRepo.cambiarEstado(nuevoEstado, codigo);
//    }
//    
}
