package gov.mintic.tiendavirtual.interfaces;

import java.util.List;

public interface Operaciones<T> {

    public List<T> consultar();

    public Boolean agregar(T miObjeto);

    public Boolean eliminar(Integer codigo);

    public Boolean actualizar(T miObjeto);

    public T buscar(Integer llavePrimaria);
    
    public Integer cantidadRegistros();
}
