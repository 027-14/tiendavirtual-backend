package gov.mintic.tiendavirtual.controladores;

import gov.mintic.tiendavirtual.entidades.Categoria;
import gov.mintic.tiendavirtual.servicios.CategoriaServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/categorias")
@CrossOrigin(origins = "*")
public class CategoriaControlador {
    
    @Autowired
    private CategoriaServicio categoServicio;
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/todas", method = RequestMethod.GET)
    public List<Categoria> mostrarCategorias() {
        return categoServicio.consultar();
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/activas", method = RequestMethod.GET)
    public List<Categoria> obtenerActivas() {
        Short estadoActivas = 1;
        return categoServicio.consultarActivas(estadoActivas);
    }
    
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/crear", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Categoria> crearCategoria(@RequestBody Categoria objCatego) {
        Short estadoInicial = 1;
        objCatego.setEstadoCategoria(estadoInicial);
        if (categoServicio.agregar(objCatego)) {
            return ResponseEntity.ok(objCatego);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @ResponseStatus(code = HttpStatus.OK, reason = "Objeto eliminado correctamente")
    @RequestMapping(value = "/borrar/{codigo}", method = RequestMethod.DELETE)
    public void borraCategoria(@PathVariable Integer codigo) {
        categoServicio.eliminar(codigo);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/uno/{codigo}", method = RequestMethod.GET)
    public Categoria obtenerUno(@PathVariable int codigo) {
        return categoServicio.buscar(codigo);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/buscar/{codigo}", method = RequestMethod.GET)
    public Categoria buscarUnaCategorias(@PathVariable Integer codigo) {
        return categoServicio.buscar(codigo);
    }
    
    @ResponseStatus(code = HttpStatus.OK, reason = "Objeto actualizado correctamente")
    @RequestMapping(value = "/editar", method = RequestMethod.PUT)
    public boolean actualizarCategorias(@RequestBody Categoria objCategoria) {
        return categoServicio.actualizar(objCategoria);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/cantidad", method = RequestMethod.GET)
    public Integer obtenerCantidad() {
        return categoServicio.cantidadRegistros();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/estado/{codigo}", method = RequestMethod.GET)
    public Integer cambiarEstado(@PathVariable Integer codigo) {
        Short estadoFinal = 1;
        Categoria objeto = categoServicio.buscar(codigo);

        if (objeto.getEstadoCategoria() == 1) {
            estadoFinal = 2;
        }
        return categoServicio.cambiarEstado(estadoFinal, codigo);
    }
}
