package gov.mintic.tiendavirtual.controladores;


import gov.mintic.tiendavirtual.entidades.Linea;
import gov.mintic.tiendavirtual.servicios.LineaServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/lineas")
@CrossOrigin(origins = "*")
public class LineaControlador {
    @Autowired
    private LineaServicio lineaServicio;
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/todas", method = RequestMethod.GET)
    public List<Linea> mostrarLineas() {
        return lineaServicio.consultar();
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/activas", method = RequestMethod.GET)
    public List<Linea> obtenerActivas() {
        Short estadoActivas = 1;
        return lineaServicio.consultarActivas(estadoActivas);
    }
    
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/crear", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Linea> crearCategoria(@RequestBody Linea objLinea) {
        Short estadoInicial = 1;
        objLinea.setEstadoLinea(estadoInicial);
        if (lineaServicio.agregar(objLinea)) {
            return ResponseEntity.ok(objLinea);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @ResponseStatus(code = HttpStatus.OK, reason = "Objeto eliminado correctamente")
    @RequestMapping(value = "/borrar/{codigo}", method = RequestMethod.DELETE)
    public void borraLinea(@PathVariable Integer codigo) {
        lineaServicio.eliminar(codigo);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/uno/{codigo}", method = RequestMethod.GET)
    public Linea obtenerUno(@PathVariable int codigo) {
        return lineaServicio.buscar(codigo);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/buscar/{codigo}", method = RequestMethod.GET)
    public Linea buscarUnaLinea(@PathVariable Integer codigo) {
        return lineaServicio.buscar(codigo);
    }
    
    @ResponseStatus(code = HttpStatus.OK, reason = "Objeto actualizado correctamente")
    @RequestMapping(value = "/editar", method = RequestMethod.PUT)
    public boolean actualizarLinea(@RequestBody Linea miObjeto) {
        return lineaServicio.actualizar(miObjeto);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/cantidad", method = RequestMethod.GET)
    public Integer obtenerCantidad() {
        return lineaServicio.cantidadRegistros();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/estado/{codigo}", method = RequestMethod.GET)
    public Integer cambiarEstado(@PathVariable Integer codigo) {
        Short estadoFinal = 1;
        Linea objeto = lineaServicio.buscar(codigo);

        if (objeto.getEstadoLinea()== 1) {
            estadoFinal = 2;
        }
        return lineaServicio.cambiarEstado(estadoFinal, codigo);

    }
    
}
