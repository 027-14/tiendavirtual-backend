package gov.mintic.tiendavirtual.controladores;

import gov.mintic.tiendavirtual.entidades.Proveedor;
import gov.mintic.tiendavirtual.servicios.ProveedorServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/proveedores")
@CrossOrigin(origins = "*")
public class ProveedorControlador {
    
    @Autowired
    private ProveedorServicio proveServicio;
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/todos", method = RequestMethod.GET)
    public List<Proveedor> mostrarProveedores() {
        return proveServicio.consultar();
    }
    
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/crear", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Proveedor> crearGenero(@RequestBody Proveedor miObjeto) {
        if (proveServicio.agregar(miObjeto)) {
            return ResponseEntity.ok(miObjeto);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @ResponseStatus(code = HttpStatus.OK, reason = "Objeto eliminado correctamente")
    @RequestMapping(value = "/borrar/{codigo}", method = RequestMethod.DELETE)
    public void borraProveedor(@PathVariable Integer codigo) {
        proveServicio.eliminar(codigo);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/uno/{codigo}", method = RequestMethod.GET)
    public Proveedor obtenerUno(@PathVariable int codigo) {
        return proveServicio.buscar(codigo);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/buscar/{codigo}", method = RequestMethod.GET)
    public Proveedor buscarUnProveedor(@PathVariable Integer codigo) {
        return proveServicio.buscar(codigo);
    }
    
    @ResponseStatus(code = HttpStatus.OK, reason = "Objeto actualizado correctamente")
    @RequestMapping(value = "/actualizar", method = RequestMethod.PUT)
    public boolean actualizarProveedor(@RequestBody Proveedor miObjeto) {
        return proveServicio.actualizar(miObjeto);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/cantidad", method = RequestMethod.GET)
    public Integer obtenerCantidad() {
        return proveServicio.cantidadRegistros();
    }

}
