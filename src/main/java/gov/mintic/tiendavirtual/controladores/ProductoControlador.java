package gov.mintic.tiendavirtual.controladores;

import gov.mintic.tiendavirtual.entidades.Producto;
import gov.mintic.tiendavirtual.servicios.ProductoServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/productos")
@CrossOrigin(origins = "*")
public class ProductoControlador {
    
    @Autowired
    private ProductoServicio proServicio;
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/todos", method = RequestMethod.GET)
    public List<Producto> mostrarProductos() {
        return proServicio.consultar();
    }
    
//    @ResponseStatus(HttpStatus.OK)
//    @RequestMapping(value = "/activas", method = RequestMethod.GET)
//    public List<Producto> obtenerActivas() {
//        Short estadoActivas = 1;
//        return proServicio.consultarActivas(estadoActivas);
//    }
    
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/crear", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Producto> crearCategoria(@RequestBody Producto objProducto) {
        Short estadoInicial = 1;
        objProducto.setEstadoProducto(estadoInicial);
        if (proServicio.agregar(objProducto)) {
            return ResponseEntity.ok(objProducto);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @ResponseStatus(code = HttpStatus.OK, reason = "Objeto eliminado correctamente")
    @RequestMapping(value = "/borrar/{codigo}", method = RequestMethod.DELETE)
    public void borraProducto(@PathVariable Integer codigo) {
        proServicio.eliminar(codigo);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/buscar/{codigo}", method = RequestMethod.GET)
    public Producto buscarUnProducto(@PathVariable Integer codigo) {
        return proServicio.buscar(codigo);
    }
    
    @ResponseStatus(code = HttpStatus.OK, reason = "Objeto actualizado correctamente")
    @RequestMapping(value = "/editar", method = RequestMethod.PUT)
    public boolean actualizarCategorias(@RequestBody Producto miObjeto) {
        return proServicio.actualizar(miObjeto);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/cantidad", method = RequestMethod.GET)
    public Integer obtenerCantidad() {
        return proServicio.cantidadRegistros();
    }

//    @ResponseStatus(HttpStatus.OK)
//    @RequestMapping(value = "/estado/{codigo}", method = RequestMethod.GET)
//    public Integer cambiarEstado(@PathVariable Integer codigo) {
//        Short estadoFinal = 1;
//        Producto objeto = proServicio.buscar(codigo);
//
//        if (objeto.getEstadoProducto()== 1) {
//            estadoFinal = 2;
//        }
//        return proServicio.cambiarEstado(estadoFinal, codigo);
//    }
//    
}
