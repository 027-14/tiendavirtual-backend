package gov.mintic.tiendavirtual.entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "linea")
public class Linea implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    @Column(name = "COD_LINEA")
    private Integer codLinea;

    @Column(name = "NOMBRE_LINEA")
    private String nombreLinea;

    @Column(name = "ESTADO_LINEA")
    private short estadoLinea;

    public Linea() {
    }

    public Linea(Integer codLinea) {
        this.codLinea = codLinea;
    }

    public Linea(Integer codLinea, String nombreLinea, short estadoLinea) {
        this.codLinea = codLinea;
        this.nombreLinea = nombreLinea;
        this.estadoLinea = estadoLinea;
    }

    public Integer getCodLinea() {
        return codLinea;
    }

    public void setCodLinea(Integer codLinea) {
        this.codLinea = codLinea;
    }

    public String getNombreLinea() {
        return nombreLinea;
    }

    public void setNombreLinea(String nombreLinea) {
        this.nombreLinea = nombreLinea;
    }

    public short getEstadoLinea() {
        return estadoLinea;
    }

    public void setEstadoLinea(short estadoLinea) {
        this.estadoLinea = estadoLinea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codLinea != null ? codLinea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Linea)) {
            return false;
        }
        Linea other = (Linea) object;
        if ((this.codLinea == null && other.codLinea != null) || (this.codLinea != null && !this.codLinea.equals(other.codLinea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.mintic.tiendavirtual.entidades.Linea[ codLinea=" + codLinea + " ]";
    }
    
}
