package gov.mintic.tiendavirtual.repositorios;

import gov.mintic.tiendavirtual.entidades.Linea;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface LineaRepositorio extends JpaRepository<Linea, Integer> {
    
    // Cantidad de registros de una tabla
    @Query("SELECT COUNT(l.codLinea) FROM Linea l")
    public Integer cantidadTotal();

    
    // Todos los registros de una tabla ordenados por un código
    @Query("SELECT l FROM Linea l ORDER BY l.codLinea")
    public List<Linea> obtenerTodas();

    
    // Registros con estado como parámetro 
    @Query("SELECT l FROM Linea l WHERE l.estadoLinea= :estado ORDER BY l.codLinea")
    public List<Linea> obtenerActivas(@Param("estado") Short valorEstado);

    
    // Update a una tabla estableciendo un campo personalizado
    @Modifying
    @Transactional
    @Query("UPDATE Linea l SET l.estadoLinea = :nuevoEstado WHERE l.codLinea = :codigo")
    public Integer cambiarEstado(@Param("nuevoEstado") Short valorNuevoEstado, @Param("codigo") Integer valorCodigo);
    
}
