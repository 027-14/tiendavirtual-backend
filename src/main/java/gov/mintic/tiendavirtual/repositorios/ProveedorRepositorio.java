package gov.mintic.tiendavirtual.repositorios;

import gov.mintic.tiendavirtual.entidades.Proveedor;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProveedorRepositorio extends JpaRepository<Proveedor, Integer> {
    
    // Cantidad de registros de una tabla
    @Query("SELECT COUNT(p.codProveedor) FROM Proveedor p")
    public Integer cantidadTotal();

    
    // Todos los registros de una tabla ordenados por un código
    @Query("SELECT p FROM Proveedor p ORDER BY p.codProveedor")
    public List<Proveedor> obtenerTodas();
    
}
