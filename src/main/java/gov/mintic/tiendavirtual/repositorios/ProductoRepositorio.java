package gov.mintic.tiendavirtual.repositorios;

import gov.mintic.tiendavirtual.entidades.Producto;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ProductoRepositorio extends JpaRepository<Producto, Integer>{
    
    // Cantidad de registros de una tabla
    @Query("SELECT COUNT(d.codProducto) FROM Producto d")
    public Integer cantidadTotal();

    
    // Todos los registros de una tabla ordenados por un código
    @Query("SELECT d FROM Producto d ORDER BY d.codProducto")
    public List<Producto> obtenerTodas();

    
//    // Registros con estado como parámetro 
//    @Query("SELECT d FROM Producto d WHERE d.estadoProducto :estado ORDER BY d.codProducto")
//    public List<Producto> obtenerActivas(@Param("estado") Short valorEstado);

    
//    // Update a una tabla estableciendo un campo personalizado
//    @Modifying
//    @Transactional
//    @Query("UPDATE Producto d SET d.estadoProducto = :nuevoEstado WHERE d.codProducto = :codigo")
//    public Integer cambiarEstado(@Param("nuevoEstado") Short valorNuevoEstado, @Param("codigo") Integer valorCodigo);
}
