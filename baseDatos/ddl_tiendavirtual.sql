/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     9/13/2022 2:20:17 PM                         */
/*==============================================================*/


alter table PRODUCTOS 
   drop foreign key FK_PRODUCTO_REFERENCE_PROVEEDO;

alter table PRODUCTOS 
   drop foreign key FK_PRODUCTO_REFERENCE_CATEGORI;

alter table PRODUCTOS 
   drop foreign key FK_PRODUCTO_REFERENCE_LINEA;

drop table if exists CATEGORIA;

drop table if exists LINEA;


alter table PRODUCTOS 
   drop foreign key FK_PRODUCTO_REFERENCE_PROVEEDO;

alter table PRODUCTOS 
   drop foreign key FK_PRODUCTO_REFERENCE_CATEGORI;

alter table PRODUCTOS 
   drop foreign key FK_PRODUCTO_REFERENCE_LINEA;

drop table if exists PRODUCTOS;

drop table if exists PROVEEDOR;

drop table if exists USUARIOS;

/*==============================================================*/
/* Table: CATEGORIA                                             */
/*==============================================================*/
create table categoria
(
   COD_CATEGORIA        int not null auto_increment  comment '',
   NOMBRE_CATEGORIA     varchar(30) not null  comment '',
   ESTADO_CATEGORIA     smallint UNSIGNED not null default 1,
   CONSTRAINT CHK_ESTADO CHECK (ESTADO_CATEGORIA>0 AND ESTADO_CATEGORIA<3),
   primary key (COD_CATEGORIA)
);

/*==============================================================*/
/* Table: LINEA                                                 */
/*==============================================================*/
create table linea
(
   COD_LINEA            int not null auto_increment  comment '',
   NOMBRE_LINEA         varchar(30) not null  comment '',
   ESTADO_LINEA         smallint UNSIGNED not null default 1,
   CONSTRAINT CHK_ESTADO CHECK (ESTADO_LINEA>0 AND ESTADO_LINEA<3),
   primary key (COD_LINEA)
);

/*==============================================================*/
/* Table: PRODUCTOS                                             */
/*==============================================================*/
create table productos
(
   COD_PRODUCTO         int not null auto_increment  comment '',
   COD_PROVEEDOR        int not null  comment '',
   COD_CATEGORIA        int not null  comment '',
   COD_LINEA            int not null  comment '',
   NOMBRE_PRODUCTO      varchar(50) not null  comment '',
   CANTIDAD_PRODUCTO    int not null  comment '',
   ESTADO_PRODUCTO      smallint UNSIGNED not null default 1,
   CONSTRAINT CHK_ESTADO CHECK (ESTADO_PRODUCTO>0 AND ESTADO_PRODUCTO<3),
   PRECIO_PRODUCTO      numeric(10,2) not null  comment '',
   IMAGEN               longtext  comment '',
   primary key (COD_PRODUCTO)
);

/*==============================================================*/
/* Table: PROVEEDOR                                             */
/*==============================================================*/
create table proveedor
(
   COD_PROVEEDOR        int not null auto_increment  comment '',
   NOMBRE_PROVEEDOR     varchar(40) not null  comment '',
   primary key (COD_PROVEEDOR)
);

/*==============================================================*/
/* Table: USUARIOS                                              */
/*==============================================================*/
/*create table USUARIOS
(
   COD_USUARIO          int not null  comment '',
   DOCUMENTO_USUARIO    varchar(10) not null  comment '',
   NOMBRES_USUARIO      varchar(30) not null  comment '',
   APELLIDOS_USUARIO    varchar(30) not null  comment '',
   TIPO_USUARIO         varchar(10) not null  comment '',
   primary key (COD_USUARIO)
);*/

alter table PRODUCTOS add constraint FK_PRODUCTO_REFERENCE_PROVEEDO foreign key (COD_PROVEEDOR)
      references PROVEEDOR (COD_PROVEEDOR) on delete restrict on update restrict;

alter table PRODUCTOS add constraint FK_PRODUCTO_REFERENCE_CATEGORI foreign key (COD_CATEGORIA)
      references CATEGORIA (COD_CATEGORIA) on delete restrict on update restrict;

alter table PRODUCTOS add constraint FK_PRODUCTO_REFERENCE_LINEA foreign key (COD_LINEA)
      references LINEA (COD_LINEA) on delete restrict on update restrict;

