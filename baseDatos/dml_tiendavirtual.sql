INSERT INTO categoria(nombre_categoria, estado_categoria) VALUES ('TERMOS Y MUG', 1);
INSERT INTO categoria(nombre_categoria, estado_categoria) VALUES ('PIÑATERIA', 1);
INSERT INTO categoria(nombre_categoria, estado_categoria) VALUES ('JUGUETERÍA', 1);
INSERT INTO categoria(nombre_categoria, estado_categoria) VALUES ('PAPELERÍA', 2);


INSERT INTO linea(nombre_linea, estado_linea) VALUES ('TERMO BEBIDA CALIENTE', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('TERMO BEBIDA FRIA', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('BOTILO PLASTICO', 2);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('BOTILO ACRILICO', 2);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('MUG TAPA ESPEJO', 2);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('MUG TAPA CERÁMICA', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('GLOBOS', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('VELAS', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('BANDEJAS', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('VASOS', 2);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('FESTONES', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('MANTELES', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('PIÑATA', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('DIDACTICOS', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('CARROS DE CONTROL', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('CARROS DE IMPULSO', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('MONTABLES', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('MUÑECAS', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('BALONES', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('PISTOLAS', 1);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('CUADERNOS', 2);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('COLORES', 2);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('LAPIZ', 2);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('LAPICERO', 2);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('BLOC', 2);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('BORRADOR', 2);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('MARCADORES', 2);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('RESMA', 2);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('REGLAS', 2);
INSERT INTO linea(nombre_linea, estado_linea) VALUES ('CARPETAS', 2);



INSERT INTO proveedor(nombre_proveedor) VALUES ('PRIMAVERA');
INSERT INTO proveedor(nombre_proveedor) VALUES ('SCRIBE');
INSERT INTO proveedor(nombre_proveedor) VALUES ('DISTRIJO');
INSERT INTO proveedor(nombre_proveedor) VALUES ('JEGZ');
INSERT INTO proveedor(nombre_proveedor) VALUES ('C&M TOYS');
INSERT INTO proveedor(nombre_proveedor) VALUES ('SURTIPIÑATAS');
INSERT INTO proveedor(nombre_proveedor) VALUES ('ARTEPARTY');
INSERT INTO proveedor(nombre_proveedor) VALUES ('CRISVA');
INSERT INTO proveedor(nombre_proveedor) VALUES ('DISTRI WOG');
INSERT INTO proveedor(nombre_proveedor) VALUES ('MARFIL');
INSERT INTO proveedor(nombre_proveedor) VALUES ('FABER CASTELL');
